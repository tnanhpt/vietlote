<?php include('./include/header_home.php') ?>
<div class="px-2 px-sm-3 py-2 count_down">
	<div class="container">
		<div class="row px-0 align-items-center">
			<div class="col-4 txt_countdown">
				<p class="count_down_title text-uppercase">Keno sẽ mở bán trong</p>
			</div>
			<div class="col-4">
				<div class="count_down_timer">
					<div class="flipper" data-reverse="true" data-datetime="2019-08-23 00:00:00" data-template="HH|ii|ss"
						data-labels="Giờ|Phút|Giây" id="myFlipper"></div>
				</div>
			</div>
			<div class="col-4 user-not-login">
				<div class="row">
					<div class="col-6">
						<a href="#" class="btn btn-outline-light btn-block">đăng ký</a>
					</div>
					<div class="col-6">
						<a href="#" class="btn btn-light btn-block">đăng nhập</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="px-2 px-sm-3 py-5 mb-5 body-content">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4 reveal">
				<div class="media p-3 bg-white bd-3">
					<img src="assets/images/left-god.png" alt="" class="mr-3">
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 1</h3>
						<p><span>10 phút</span> quay thưởng <span>1 lần</span></p>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2">
					<a href="#"><i class="fas fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
			<div class="col-12 col-md-4 reveal">
				<div class="media p-3 bg-white bd-3">
					<img src="assets/images/left-god.png" alt="" class="mr-3">
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 2</h3>
						<p>Cơ hội trúng<span> 2 tỷ đồng</span> chỉ với <span>10k VNĐ</span></p>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2">
					<a href="#"><i class="fas fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
			<div class="col-12 col-md-4 reveal">
				<div class="media p-3 bg-white bd-3">
					<img src="assets/images/left-god.png" alt="" class="mr-3">
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 3</h3>
						<p class="text-uppercase"><span>Không Trùng Vẫn Trúng</span></p>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2">
					<a href="#"><i class="fas fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- <div class="col-md-4">
				<img src="./assets/images/home/banner-1.png" alt="" class="img-fluid">
			</div>
			<div class="col-md-4">
				<img src="./assets/images/home/banner-2.png" alt="" class="img-fluid">
			</div>
			<div class="col-md-4">
				<img src="./assets/images/home/banner-3.png" alt="" class="img-fluid">
			</div> -->
		</div>

		<div class="row">
			<div class="col-xl-8">
				<div class="home_left">
					<img src="./assets/images/home/choi-thu-trung-that.png" class="img-fluid" />
					<div class="action">
						<a class="btn btn-base btn-play" href="#">CHƠI NGAY</a>
					</div>
				</div>
			</div>
			<div class="col-xl-4">
				<div class="home_right">
					<img src="./assets/images/home/xo-so-keno.png" class="img-fluid" />
					<div class="action">
						<a class="btn btn-base btn-play" href="#">TÌM HIỂU NGAY</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-8">
				<div class="bd-3 p-4 h-100">
					<div class="row align-items-center">
						<div class="col-3 text-center">
							<img src="assets/images/map-location.png" alt="" class="img-fluid">
						</div>
						<div class="col-6">
							<h5 class="">Tìm điểm bán Keno & Đổi thưởng</h5>
							<p>Vui lòng chọn <span class="text-or">"Tìm Điểm Bán"</span> và xác nhận <span class="text-or">"Cho
									phép"</span> ở góc trái màn hình để được hướng dẫn đến điểm bán gần nhất</p>
						</div>
						<div class="col-3 ">
							<a href="#" class="btn btn-keno btn-block text-uppercase">Tìm Điểm Bán</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="bd-3 p-3 h-100">
					<div class="row h-100 align-items-center">
						<div class="col-7">
							<div class="">Thể lệ chơi Keno tại điểm bán</div>
						</div>
						<div class="col-5">
							<img src="./assets/images/common/Logo.png" class="img-fluid" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bd-3 p-3">
			<div class="row align-items-center">
				<div class="col-10">
					<div class="row align-items-center">
						<div class="col-5">
							<h3>Kết quả dò Keno</h3>
							<p class="m-0">Vui lòng kiểm tra "Ngày quay thưởng" và "Kỳ quay thưởng" trên vé để kiểm tra kết quả</p>
						</div>
						<div class="col-7">
							<div class="row">
								<div class="col-6">
									<select class="form-control" id="exampleFormControlSelect1">
										<option>Ngày quay thưởng</option>
									</select>
								</div>
								<div class="col-6">
									<select class="form-control" id="exampleFormControlSelect1">
										<option>Kỳ quay thưởng</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-2">
					<button class="btn btn-keno btn-block">Dò kết quả</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-8">
				<div class="bd-3 h-100 p-3">
					<h6 class="text-uppercase">Giải trí</h6>
				</div>
			</div>
			<div class="col-4">
				<div class="bd-3 h-100 p-3">
					<h6 class="text-uppercase">Câu hỏi thường gặp</h6>
					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h2 class="mb-0">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
										aria-expanded="true" aria-controls="collapseOne">
										Dễ chơi dễ trúng quá!
									</button>
								</h2>
							</div>

							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
									moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
									Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
									shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
									proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
									aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h2 class="mb-0">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo"
										aria-expanded="false" aria-controls="collapseTwo">
										Collapsible Group Item #2
									</button>
								</h2>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
								<div class="card-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
									moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
									Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
									shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
									proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
									aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree">
								<h2 class="mb-0">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse"
										data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Collapsible Group Item #3
									</button>
								</h2>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
								<div class="card-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
									moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
									Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
									shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
									proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
									aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="other-products">
			<div class="text-center">
				<h1 class="text-or">Các sản phẩm khác</h1>
				<button class="btn btn-keno px-5 text-uppercase">tìm hiểu thêm</button>
				<p>Các vé trúng thưởng có các số trùng với kết quả và không cần theo thứ tự</p>
				<div class="row">
					<div class="col-3">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex align-items-center text-uppercase text-or font-weight-bold mb-3">Giải thưởng cộng dồn bắt đầu từ 30 Tỷ</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/power.png" class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chọn 6 số bất kì trong 55 số từ 01-55</h6>
							<a href="#" class="btn btn-keno px-4 text-uppercase">xem thêm</a>
						</div>
					</div>
					<div class="col-3">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex align-items-center text-uppercase text-or font-weight-bold mb-3">Giải thưởng cộng dồn bắt đầu từ 12 Tỷ</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/mega.png" class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chọn 6 số bất kì trong 45 số từ 01-45</h6>
							<a href="#" class="btn btn-keno px-4 text-uppercase">xem thêm</a>
						</div>
					</div>
					<div class="col-3">
							<div class="bd-3 h-100 p-3">
								<h6 class="other-products_title d-flex align-items-center text-uppercase text-or font-weight-bold mb-3">Giải thưởng cố định</h6>
								<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/max_3d.png" class="img-fluid" alt=""></div>
								<h6 class="other-products_desc my-3 font-weight-bold">Chỉ cần chọn 4 số</h6>
								<a href="#" class="btn btn-keno px-4 text-uppercase">xem thêm</a>
							</div>
						</div>
						<div class="col-3">
								<div class="bd-3 h-100 p-3">
									<h6 class="other-products_title d-flex align-items-center text-uppercase text-or font-weight-bold mb-3">Giải thưởng cố định</h6>
									<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/max_4d.png" class="img-fluid" alt=""></div>
									<h6 class="other-products_desc my-3 font-weight-bold">Chỉ cần chọn 3 số</h6>
									<a href="#" class="btn btn-keno px-4 text-uppercase">xem thêm</a>
								</div>
							</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-xl-8">
				<div class="home_left">
					<iframe class="home_left" style="height:472px" src="https://www.youtube.com/embed/Jr51eN3Z57M" frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-xl-4 slider_home home_right">
				<div id="carouselId" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselId" data-slide-to="0" class="active"></li>
						<li data-target="#carouselId" data-slide-to="1"></li>
						<li data-target="#carouselId" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
							<div class="carousel-caption">
								<div class="tip_caption">
									Có khi BB trúng 2 tỷ thì mọi người sẽ không thấy BB trên các
									phương tiện thông tin đại chúng một thời gian :)))
								</div>
								<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
							</div>
						</div>
						<div class="carousel-item">
							<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
							<div class="carousel-caption">
								<div class="tip_caption">
									Lorem ipsum dolor sit amet consectetur adipisicing elit.
									Quibusdam pariatur cumque debitis quidem suscipit earum nihil a.
									Fugiat, molestiae ipsum.
								</div>
								<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
							</div>
						</div>
						<div class="carousel-item">
							<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
							<div class="carousel-caption">
								<div class="tip_caption">
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
									odio tenetur iure natus eveniet quo rem aperiam adipisci sit
									optio?
								</div>
								<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
		<div class="agency px-3">
			<div class="row my-4 text-center w-100">
				<p class="title_block fw-600 w-100 text-or">Keno - Chơi càng to càng vui</p>
			</div>
			<div class="row mt-3 shadow bg-white align-items-center mark">
				<div class="col-md-6 p-3">
					<h3>Bạn gặp vấn đề...</h3>
					<p>Muốn tìm một trò chơi mới cho buổi hoạt náo hàng tuần của công ty ?</p>
					<p> Tìm kiếm một hoạt động giải trí để thu hút KH cho cửa hàng của bạn ?</p>
					<p>Tìm kiếm một trò chơi cho sự kiện của bạn?</p>
					<a class="text-or fz-24 fw-600" href="#">
						<i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i> KENO sẽ giúp bạn giải quyết
					</a>
				</div>
				<div class="col-md-6">
					<img src="./assets/images/agency/question.png" alt="" class="img-fluid">
				</div>
			</div>
			<div class="row mt-3 shadow bg-white align-items-center px-0">
				<div class="col-md-6 px-0">
					<img src="./assets/images/agency/game.png" alt="" class="img-fluid">
				</div>
				<div class="col-md-6">
					<h3>Trải nghiệm game KENO</h3>
					<p> Mới lạ đầy thú vị</p>
					<p>Chơi thử nhận quà thật</p>
					<p>Quà nhỏ giải thưởng to lên đến 2 tỷ đồng</p>
					<p>Vừa tạo trò chơi giải trí, vừa có thể đem quà mang về</p>
					<div class="btn btn-fontCancel">TÌM HIỂU NGAY</div>
				</div>
			</div>
			<div class="row mt-3 shadow bg-white px-0 map_picker">
				<div class="col-md-4 px-0">
					<div class="title_map">Danh sách các điểm bán</div>
					<div class="px-3">

						<!-- //start map show is mobile -->
						<div class="map_mobile mt-3">
							<iframe
								src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
								width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>

						<!-- end map show is mobile -->
						<div class="mt-3 d-flex justify-content-between align-items-center">
							<div class="form-group">
								<select class="form-control" name="" id="">
									<option selected disabled value="">Tỉnh/ Thành phố</option>
									<option value="Thái Nguyên">Thái Nguyên</option>
									<option value="Hà Nội">Hà Nội</option>
								</select>
							</div>
							<div class="form-group">
								<select class="form-control" name="" id="">
									<option selected disabled value="">Quận/ Huyện</option>
									<option value="Quận 6">Quận 6</option>
									<option value="Quận 7">Quận 7</option>
								</select>
							</div>
						</div>
						<div class="result">
							<ul>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
								<li>
									<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
									<p>123 Đường ABC, Phường X, Quận Z, HN </p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- show is not mobile -->
				<div class="col-md-8 map_desktop">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
						width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<!-- end show is not mobile -->
			</div>


			<div class="row mt-4 px-0">
				<div class="col-12 px-0">
					<img src="./assets/images/home/banner_bottom.png" alt="" class="img-fluid d-none d-md-block" />
					<img src="./assets/images/home/banner_bottom_mobile.png" alt="" class="img-fluid d-block d-md-none" />
				</div>
			</div>
			<div class="row my-4 text-center w-100">
				<p class="title_block fw-600 w-100 text-or">Tìm điểm bán để trải nghiệm ngay</p>
			</div>
		</div>

	</div>
</div>

<?php include('./include/footer.php')  ?>