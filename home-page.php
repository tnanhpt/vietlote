<?php include('./include/header_home.php') ?>
<div class="px-2 pt-2 py-lg-2 count_down menu_top">
	<div class="container">
		<div class="row px-0 align-items-center">
			<div class="col-auto mr-auto d-block d-lg-none menu-toggle">
				<button class="navbar-toggler p-0 d-block d-lg-none border-0" type="button" data-toggle="offcanvas"
					style="color: #fff">
					<i class="fas fa-bars fa-lg"></i>
				</button>
			</div>
			<div class="col-lg-4 col-9 user-not-login">
				<div class="row">
					<div class="col-6">
						<a href="#" class="btn btn-outline-light btn-sm btn-block">đăng ký</a>
					</div>
					<div class="col-6 pl-0 pl-xl-3">
						<a href="/home-page-logged.php" class="btn btn-light btn-sm btn-block">đăng nhập</a>
					</div>
				</div>
			</div>
			<div class="pl-0 pl-sm-3 pt-3 pt-lg-0 col-lg-8 order-lg-first">
				<div class="row align-items-center">
					<div class="col-auto pl-0 pl-sm-3 d-block d-lg-none">
						<img src="assets/images/home/logo-xo-so-nhanh.png" alt="">
					</div>
					<div class="col text-center">
						<div class="row justify-content-center align-items-center">
							<div class="col-sm-6 text-lg-left txt_countdown">
								<p class="count_down_title text-uppercase d-none d-md-block">
									Keno Xổ, Nổ niềm vui <br>
									<small>Keno sẽ mở bán trong</small></p>
								<p class="count_down_title text-uppercase d-block d-md-none">Keno sẽ mở bán trong</p>
							</div>
							<div class="col-12 col-sm-6 px-xl-5 mb-1">
								<div class="count_down_timer">
									<div class="flipper" data-reverse="true" data-datetime="2019-08-23 00:00:00" data-template="HH|ii|ss"
										data-labels="Giờ|Phút|Giây" id="myFlipper"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="px-2 px-sm-3 py-4 py-md-5 mb-5 body-content">
	<div class="container">
		<div class="d-block d-sm-none mb-4 social-m">
			<div class="row align-items-center">
				<div class="col-auto">
					<img src="assets/images/home/god-hands.png" class="img-fluid" alt="">
					<h6 class="font-utm-azuki text-center m-0 text-or">Đổi thưởng</h6>
				</div>
				<div class="col">
					<a href="#" class="btn btn-keno btn-lg btn-block mb-3"><i class="fab fa-facebook fa-lg fa-fw"></i> Chia sẻ</a>
					<a href="#" class="btn btn-keno btn-lg btn-block"><i class="fa fa-map-marker-alt fa-lg fa-fw"></i> Tìm điểm
						bán</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 reveal mb-3 mb-md-0">
				<div class="media px-3 py-2 bg-white bd-3">
					<div class="d-none d-md-block">
						<img src="assets/images/left-god.png" alt="" class="mr-3 img-fluid">
					</div>
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 1</h3>
						<p class="m-0"><span>10 phút</span> quay thưởng <span>1 lần</span></p>
					</div>
					<div class="align-self-center d-block d-md-none">
						<a href="#" class="btn btn-keno btn-sm ml-3"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2 d-none d-md-block">
					<a href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 reveal mb-3 mb-md-0">
				<div class="media px-3 py-2 bg-white bd-3">
					<div class="d-none d-md-block">
						<img src="assets/images/left-god.png" alt="" class="mr-3 img-fluid">
					</div>
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 2</h3>
						<p class="m-0">Cơ hội trúng<span> 2 tỷ đồng</span> chỉ với <span>10k VNĐ</span></p>
					</div>
					<div class="align-self-center d-block d-md-none">
						<a href="#" class="btn btn-keno btn-sm ml-3"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2 d-none d-md-block">
					<a href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 reveal mb-4 mb-md-0">
				<div class="media px-3 py-2 bg-white bd-3">
					<div class="d-none d-md-block">
						<img src="assets/images/left-god.png" alt="" class="mr-3 img-fluid">
					</div>
					<div class="media-body">
						<h3 class="reveal_title mb-1">Bật mí 3</h3>
						<p class="m-0 text-uppercase"><span>Không Trùng Vẫn Trúng</span></p>
					</div>
					<div class="align-self-center d-block d-md-none">
						<a href="#" class="btn btn-keno btn-sm ml-3"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
					</div>
				</div>
				<div class="reveal_f text-right px-3 py-2 d-none d-md-block">
					<a href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
				</div>
			</div>
		</div>
		<div class="play-now mb-4">
			<div class="row">
				<div class="col-xl-8">
					<div class="play-hit h-100 d-none d-xl-block">
						<a href="#" class="w-100 h-100 d-block"></a>
					</div>
					<div class="d-none d-sm-block d-xl-none">
						<img src="./assets/images/home/choi-thu-trung-that-2.png" class="img-fluid" />
					</div>
					<div class="d-block d-sm-none">
						<img src="./assets/images/home/choi-thu-trung-that_mobile.png" class="img-fluid" />
					</div>
				</div>
				<div class="col-xl-4">
					<div class="bd-3 rules-trial-play">
						<!-- <img src="./assets/images/home/xo-so-keno.png" class="img-fluid" /> -->
						<h3 class="rules-trial-play_title">Thể lệ chơi thử<br>và cách đổi thưởng</h3>
						<div class="p-3">
							<div class="mb-3">
								<h4 class="font-utm-azuki">3 bước chơi thử</h4>
								<a class="btn btn-keno btn-sm" href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
							</div>
							<div class="mb-3">
								<h4 class="font-utm-azuki">Cơ cấu giải thưởng</h4>
								<a class="btn btn-keno btn-sm" href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
							</div>
							<div>
								<h4 class="font-utm-azuki">Tích điểm đổi thưởng</h4>
								<a class="btn btn-keno btn-sm" href="#"><i class="fa fa-angle-double-right"></i> Xem thêm</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="sale-point mb-4">
			<div class="row mb-0 mb-md-4">
				<div class="col-md-8 mb-3 mb-md-0">
					<div class="bd-3 px-3 py-2 h-100">
						<div class="row align-items-md-center">
							<div class="col-3 col-xl-3 text-center">
								<img src="assets/images/map-location.png" alt="" class="img-fluid">
							</div>
							<div class="col-9 col-xl-6">
								<h4 class="font-utm-azuki">Tìm điểm bán Keno & Đổi thưởng</h4>
								<p>Vui lòng chọn <span class="text-or">"Tìm Điểm Bán"</span> và xác nhận <span class="text-or">"Cho
										phép"</span> ở góc trái màn hình để được hướng dẫn đến điểm bán gần nhất</p>
							</div>
							<div class="col-xl-3">
								<a href="#" class="btn btn-keno btn-block text-uppercase">Tìm Điểm Bán</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="bd-3 p-3 h-100">
						<div class="row h-100 align-items-center">
							<div class="col-xl-7">
								<h4 class="font-utm-azuki text-center text-md-left">Thể lệ chơi Keno tại điểm bán</h4>
							</div>
							<div class="col-xl-5">
								<img src="/assets/images/home/banner-10-ty.png" alt="" class="img-fluid d-block d-md-none m-auto">
								<img src="./assets/images/common/Logo.png" class="img-fluid d-none d-md-block" alt="">
								<a href="#" class="btn btn-keno btn-block text-uppercase mt-3">Xem ngay</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bd-3 p-3 result-keno">
				<div class="row align-items-center">
					<div class="col-md-10">
						<div class="row align-items-center">
							<div class="col-md-5">
								<h3 class="font-utm-azuki">Kết quả dò Keno</h3>
								<p class="m-0">Vui lòng kiểm tra <span class="text-or">"Ngày quay thưởng"</span> và <span
										class="text-or">"Kỳ quay thưởng"</span> trên vé để kiểm tra kết quả</p>
							</div>
							<div class="col-md-7 my-3 my-md-0">
								<div class="row">
									<div class="col-lg-6 mb-3 mb-lg-0">
										<select class="custom-select">
											<option>NGÀY QUAY THƯỞNG</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select class="custom-select">
											<option>KỲ QUAY THƯỞNG</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<button class="btn btn-keno btn-block text-uppercase">Dò kết quả</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-lg-8 mb-4 mb-lg-0 entertainment">
				<div class="bd-3 h-100">
					<h6 class="entertainment_title text-uppercase p-3">Giải trí</h6>
					<div class="p-3">						
						<div class="slider variable-width">
							<div style="width: 200px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
							<div style="width: 175px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
							<div style="width: 150px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
							<div style="width: 300px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
							<div style="width: 225px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
							<div style="width: 125px;">
								<div class="m-2"><svg width="100%" height="300">
										<rect width="100%" height="100%" fill="#868e96"></rect>
									</svg></div>
							</div>
						</div>
						<!-- <ul class="list-inline">
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
								<li class="list-inline-item"><svg width="250" height="300"><rect width="100%" height="100%" fill="#868e96"></rect></svg></li>						
							</ul>		 -->
					</div>
				</div>
			</div>
			<div class="col-lg-4 question">
				<div class="bd-3 h-100">
					<h6 class="question_title text-uppercase p-3">Câu hỏi thường gặp</h6>
					<div class="p-3">
						<div class="accordion" id="accordionExample">
							<div class="card">
								<div class="card-header p-0" id="headingOne">
									<h2 class="mb-0">
										<a href="#" class="btn btn-link btn-block px-0" data-toggle="collapse" data-target="#collapseOne">
											Dễ chơi dễ trúng quá!
											<i class="fa fa-angle-up"></i>
										</a>
									</h2>
								</div>
								<div id="collapseOne" class="collapse show" data-parent="#accordionExample">
									<div class="card-body p-0">
										<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
											wolf
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
											eiusmod.</p>
										<p>Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
											assumenda
											shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
											sapiente
											ea
											proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw
											denim
											aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header p-0" id="headingTwo">
									<h2 class="mb-0">
										<a href="#" class="btn btn-link btn-block px-0 collapsed" data-toggle="collapse"
											data-target="#collapseTwo">
											Collapsible Group Item #2
											<i class="fa fa-angle-down"></i>
										</a>
									</h2>
								</div>
								<div id="collapseTwo" class="collapse" data-parent="#accordionExample">
									<div class="card-body p-0">
										<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
											wolf
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
											eiusmod.</p>
										<p>Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
											assumenda
											shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
											sapiente
											ea
											proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw
											denim
											aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="other-products">
			<div class="text-center">
				<h1 class="text-or mb-3">Các sản phẩm khác</h1>
				<button class="btn btn-keno px-5 text-uppercase">tìm hiểu thêm</button>
				<h6 class="mt-3 mb-4">Các vé trúng thưởng có các số trùng với kết quả và không cần theo thứ tự</h6>
				<div class="row">
					<div class="col-6 col-xl-3 mb-4 mb-xl-0">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex justify-content-center align-items-center mb-3">
								Giải thưởng cộng dồn bắt đầu từ 30 Tỷ</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/power.png"
									class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chọn 6 số bất kì trong 55 số từ 01-55</h6>
							<a href="#" class="btn btn-keno px-sm-5 px-xl-4 text-uppercase">xem thêm</a>
						</div>
					</div>
					<div class="col-6 col-xl-3 mb-4 mb-xl-0">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex justify-content-center align-items-center mb-3">
								Giải thưởng cộng dồn bắt đầu từ 12 Tỷ</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/mega.png"
									class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chọn 6 số bất kì trong 45 số từ 01-45</h6>
							<a href="#" class="btn btn-keno px-sm-5 px-xl-4 text-uppercase">xem thêm</a>
						</div>
					</div>
					<div class="col-6 col-xl-3 mb-4 mb-xl-0">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex justify-content-center align-items-center mb-3">
								Giải thưởng cố định</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/max_3d.png"
									class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chỉ cần chọn 4 số</h6>
							<a href="#" class="btn btn-keno px-sm-5 px-xl-4 px-xl-4 text-uppercase">xem thêm</a>
						</div>
					</div>
					<div class="col-6 col-xl-3 mb-4 mb-xl-0">
						<div class="bd-3 h-100 p-3">
							<h6 class="other-products_title d-flex justify-content-center align-items-center mb-3">
								Giải thưởng cố định</h6>
							<div class="other-products_img d-flex align-items-center"><img src="assets/images/products/max_4d.png"
									class="img-fluid" alt=""></div>
							<h6 class="other-products_desc my-3 font-weight-bold">Chỉ cần chọn 3 số</h6>
							<a href="#" class="btn btn-keno px-sm-5 px-xl-4 px-xl-4 text-uppercase">xem thêm</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('./include/footer.php')  ?>