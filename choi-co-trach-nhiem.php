<?php include('./include/header.php') ?>
<div class="px-2 px-sm-3 py-4 py-md-5 mb-5 body-content">
  <div class="container responsibility">
    <h1 class="text-or mb-4">Chơi có trách nhiệm</h1>
    <div class="card-columns">
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/1.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Tổng hợp giải Jackpot của Mỹ</h4>
          <p class="card-text">Cùng admin điểm qua 1 vòng từ năm 2014 đến 2019 của 2 giải Jackpot lớn nhất
            của Mỹ trong Infographic phía...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            25/06/2019 06:47 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/2.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Infographic 9 lưu ý "chơi có trách nhiệm"</h4>
          <p class="card-text">Cùng admin điểm qua 1 vòng từ năm 2014 đến 2019 của 2 giải Jackpot lớn nhất
            của Mỹ trong Infographic phía...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            17/04/2019 09:51.responsibility </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/3.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Vietlott tiên phong trong chương trình “Chơi có trách nhiệm”</h4>
          <p class="card-text">Là doanh nghiệp tiên phong trong việc ứng dụng công nghệ hiện đại vào kinh
            doanh xổ số từ khâu phát hành vé, quay số mở thưởng và...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            25/06/2019 06:47 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/4.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Nên làm gì nếu bỗng dưng trúng xổ số độc đắc?</h4>
          <p class="card-text">Một khi vận may mỉm cười với bạn, hãy sử dụng vận may ấy một cách thông
            mình. Sau đây là một số những lưu ý bạn nên làm nếu ....</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            24/09/2018 09:00 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/5.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Những giải thưởng xổ số lớn nhất thế giới</h4>
          <p class="card-text">Đa số những giải thưởng xổ số lớn nhất thế giới đều là những giải xổ số tự
            chọn với hình thức cộng dồn giải thưởng, khiến giá trị ...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            17/04/2019 09:51 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/6.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Hãy chơi có trách nhiệm</h4>
          <p class="card-text">Chúng tôi muốn các bạn hãy tham gia các trò chơi một cách có trách nhiệm và
            duy trì trong giới hạn vừa phải, vì vậy các bạn hãy xem ...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            25/06/2019 06:47 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/7.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Một số lời khuyên</h4>
          <p class="card-text">Cho người tham gia các trò chơi giải trí có thưởng</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            24/09/2018 09:00 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/7.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Mười lời khuyên dành cho người trúng số</h4>
          <p class="card-text">Dưới đây chúng tôi tổng kết ra 10 lời khuyên sử dụng tiền trúng thưởng xổ
            số hiệu quả nhất dành cho người trúng số:</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            24/09/2018 09:00 </p>
        </div>
      </div>
      <div class="card shadow">
        <a href="chi-tiet-trach-nhiem.php"><img class="card-img-top" src="./assets/images/news/9.png" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">Thuế thu nhập cá nhân đối với thu nhập từ trúng thưởng xổ số</h4>
          <p class="card-text">Khi chơi xổ số, có rất nhiều khách hàng thắc mắc về thuế, phí đối với các
            khoản tiền trúng thưởng. Công ty Xổ số Điện toán Việt Nam ...</p>
          <p class="date_txt">
            <i class="fas fa-calendar fa-lg mr-2"></i>
            24/09/2018 09:00 </p>
        </div>
      </div>
    </div>
    <!-- start row -->
    <div class="row">
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
      <!-- card news -->
      <div class="col-lg-4 col-md-6">
      </div>
      <!-- end card news -->
    </div>
    <!-- end row -->
    <div class="w-100 text-center mt-5">
      <a href="#" class="btn btn-load-more px-5">Xem thêm</a>
    </div>
  </div>
</div>
<?php include('./include/footer.php')  ?>