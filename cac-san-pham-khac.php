<?php include('./include/header.php') ?>
<div class="px-2 px-sm-3 py-4 py-md-5 mb-5 body-content">
  <div class="container other_products">
    <h1 class="mb-4 text-or">Các sản phẩm khác</h1>
    <div class="px-3">
    <!-- start product -->
    <div class="row shadow bg-white mb-4 py-4">
      <div class="col-md-6 image_box">
        <img src="./assets/images/products/power.png" alt="" class="img-fluid">
      </div>
      <div class="col-md-6">
        <h2>Power 6/55</h2>
        <p>Chỉ từ 10.000 đồng, chọn 6 số từ 01-55 để có cơ hội trúng thưởng Jackpot 1 từ 30 tỷ đồng,
          Jackpot 2 từ 3 tỷ đồng.
          POWER 6/55 quay số mở thưởng vào 18h00 các ngày thứ 3, thứ 5 và thứ 7 hàng tuần</p>
        <a href="cac-san-pham-khac_chi-tiet.php" class="btn btn-fontCancel">TÌM HIỂU NGAY</a>
      </div>
    </div>
    <!-- end product -->
    <!-- start product -->
    <div class="row shadow bg-white mb-4 py-4">
      <div class="col-md-6 image_box">
        <img src="./assets/images/products/mega.png" alt="" class="img-fluid">
      </div>
      <div class="col-md-6">
        <h2>Mega 6/45</h2>
        <p>Chỉ từ 10.000 đồng, chọn 6 số từ 01-45 để có cơ hội trúng thưởng Jackpot từ 12 tỷ đồng.
          MEGA 6/45 quay số mở thưởng vào 18h00 các ngày thứ 4, thứ 6 và Chủ Nhật hàng tuần.</p>
        <a href="cac-san-pham-khac_chi-tiet.php" class="btn btn-fontCancel">TÌM HIỂU NGAY</a>
      </div>
    </div>
    <!-- end product -->
    <!-- start product -->
    <div class="row shadow bg-white mb-4 py-4">
      <div class="col-md-6 image_box">
        <img src="./assets/images/products/max_3d.png" alt="" class="img-fluid">
      </div>
      <div class="col-md-6">
        <h2>Max 3D</h2>
        <p>Chỉ từ 10.000 đồng, chọn 3 chữ số từ 000-999 để có cơ hội trúng thưởng nhận các giải thưởng
          hấp dẫn.
          Max 3D quay số mở thưởng vào 18h00 các ngày thứ 2, thứ 4 và thứ 6 hàng tuần.</p>
        <a href="cac-san-pham-khac_chi-tiet.php" class="btn btn-fontCancel">TÌM HIỂU NGAY</a>
      </div>
    </div>
    <!-- end product -->
    <!-- start product -->
    <div class="row shadow bg-white mb-4 py-4">
      <div class="col-md-6 image_box">
        <img src="./assets/images/products/max_4d.png" alt="" class="img-fluid">
      </div>
      <div class="col-md-6">
        <h2>Max 4D</h2>
        <p>Chỉ từ 10.000 đồng, chọn 4 chữ số từ 0000-9999 để có cơ hội trúng thưởng lên tới 1.500 lần.
          Max 4D quay số mở thưởng vào 18h00 các ngày thứ 3, thứ 5 và thứ 7 hàng tuần.</p>
        <a href="cac-san-pham-khac_chi-tiet.php" class="btn btn-fontCancel">TÌM HIỂU NGAY</a>
      </div>
    </div>
  </div>
    <!-- end product -->
  </div>
</div>
<?php include('./include/footer.php')  ?>