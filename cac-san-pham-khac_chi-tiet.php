<?php include('./include/header.php') ?>
<div class="px-2 px-sm-3 py-4 py-md-5 mb-5 body-content">
	<div class="container other_products">
		<h1 class="mb-4 text-or">Các sản phẩm khác</h1>
		<div class="bg-white other_products-header p-3 p-md-5 mb-4">
			<div class="text-center">
				<img src="assets/images/products/power.png" alt="" class="img-fluid">
				<h3 class="mt-4">Power 6/55</h3>
				<p class="px-xl-5 m-0">Chỉ từ <span class="text-or">10.000 đồng</span>, chọn <span class="text-or">6 số từ
						01-55</span> để có cơ hội trúng thưởng Jackpot 1 từ <span class="text-or">30 tỷ đồng</span>, Jackpot
					<span class="text-or">2 từ 3 tỷ đồng</span>.
					<span class="text-or">POWER 6/55</span> quay số mở thưởng vào <span class="text-or">18h00 các
						ngày</span> <span class="text-or">thứ 3, thứ 5 và thứ 7 hàng tuần</span>.</p>
			</div>
		</div>
		<div class="bg-white px-3 px-xl-5">
			<div class="py-3 px-lg-5 mx-xl-5">
				<h2 class="text-center text-or my-4">Cơ cấu giải thưởng</h2>
				<p><b>1. Gồm 5 hạng giải thưởng</b> và quay số mở thưởng 01 lần trong mỗi kỳ quay số mở thưởng để lựa chọn ra bộ
					số trúng thưởng gồm 6 số trong các số từ 01 đến 55. Ngoài ra, một số đặc biệt sẽ được quay chọn từ 49 quả bóng
					còn lại trong lồng cầu sau khi đã chọn 6 quả bóng trước đó để xác định bộ số trúng thưởng cho giải Jackpot 2.
				</p>
				<p><b>2. Cơ cấu giải thưởng cụ thể như sau:</b></p>
				<div class="table-responsive">
					<table class="table table-hover table-st riped table-result">
						<thead class="thead-dark">
							<tr>
								<th>Giải thưởng</th>
								<th>Kết quả</th>
								<th class="text-right">Giá trị giải (đồng)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jackpot 1</td>
								<td class="text-or" nowrap=""><b>O O O O O O</b></td>
								<td class="text-right"><b>(Tối thiểu 30 tỷ và tích lũy)</b></td>
							</tr>
							<tr>
								<td>Jackpot 2</td>
								<td class="text-or" nowrap=""><b>O O O O O | <span class="active">O</span></b></td>
								<td class="text-right"><b>(Tối thiểu 3 tỷ và tích lũy)</b></td>
							</tr>
							<tr>
								<td>Giải Nhất</td>
								<td class="text-or" nowrap=""><b>O O O O O</b></td>
								<td class="text-right"><b>40.000.000</b></td>
							</tr>
							<tr>
								<td>Giải Nhì</td>
								<td class="text-or" nowrap=""><b>O O O O</b></td>
								<td class="text-right"><b>500.000</b></td>
							</tr>
							<tr>
								<td>Giải Ba</td>
								<td class="text-or" nowrap=""><b>O O O</b></td>
								<td class="text-right"><b>50.000</b></td>
							</tr>
						</tbody>
					</table>
				</div>
				<p><em><b>Ghi chú:</b> <span class="text-or">O</span> là các số trùng với kết quả quay số mở thưởng, không theo
						thứ tự</em></p>
				<ul>
					<li>Trong trường hợp vé của người trúng thưởng trúng nhiều hạng giải thưởng thì người trúng thưởng chỉ được
						lĩnh một hạng giải thưởng cao nhất.</li>
					<li>Trong trường hợp có nhiều người trúng thưởng giải Jackpot thì giải Jackpot được chia đều theo tỷ lệ giá
						trị tham gia dự thưởng của người trúng thưởng.</li>
					<li>Giá trị lĩnh thưởng của các giải thưởng từ Giải Nhất đến Giải Ba được tính theo số lần tham gia dự thưởng
						của số trúng thưởng (01 lần tham gia dự thưởng mệnh giá 10.000 đồng) nhân với giá trị giải thưởng tương ứng
						với 01 lần tham gia dự thưởng.</li>
				</ul>
				<h2 class="mt-5 text-or text-center">Trúng thưởng nhiều hơn qua chơi bao</h2>
				<p class="text-center">Xem hướng dẫn chơi bao <a href="#">tại đây</a></p>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 5 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 2 SỐ</td>
									<td class="text-right">200.000</td>
								</tr>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">3.850.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">104.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">(JACKPOT 2*x2)<br>+ 24 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 1,920 Tỷ Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 7 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">200.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">3.850.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">104.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">(JACKPOT 2*)<br>+ 42, Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 240 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 8 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">500.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">3.800.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">128.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*)<br>+ 88 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 487,5 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 247,5 Triệu Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 9 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">1.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">7.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">177.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 137 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 743,5 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 503,5 Triệu Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 12 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">4.200.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">25.200.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">350.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 310 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 1,57 Tỷ Đồng**</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 1,33 Tỷ Đồng**</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 13 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">6.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">34.800.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">418.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 378 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 1,87 Tỷ Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 1,63 Tỷ Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 14 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">8.250.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">46.500.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">492.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 452 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 2,18 Tỷ Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 1,94 Tỷ Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 15 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">11.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">60.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">672.500.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 532,5 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 2,51 Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 2,27 Tỷ Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<table class="table table-hover table-striped table-bao">
							<thead class="thead-dark">
								<tr>
									<th colspan="2" class="text-center">BAO 18 (VNĐ)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>TRÚNG 3 SỐ</td>
									<td class="text-right">22.750.000</td>
								</tr>
								<tr>
									<td>TRÚNG 4 SỐ</td>
									<td class="text-right">118.300.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ</td>
									<td class="text-right">858.000.000</td>
								</tr>
								<tr>
									<td>TRÚNG 5 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 2*<br>+ 818 Triệu Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ</td>
									<td class="text-right">JACKPOT 1*<br>+ 3,59 Tỷ Đồng</td>
								</tr>
								<tr>
									<td>TRÚNG 6 SỐ +<br>BONUS NUMBER</td>
									<td class="text-right">JACKPOT 1*<br>+ JACKPOT 2*<br>+ 3,35 Tỷ Đồng</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<ul class="list-unstyled">
					<li><b>**</b> : Làm tròn đến 2 chữ số sau số thập phân</li>
					<li><b>Jackpot 1*:</b> Một phần chia của giải Jackpot 1</li>
					<li><b>Jackpot 2*:</b> Có nghĩa là Jackpot 2 được chia thành 6 phần đều nhau . Nếu không có người trúng
						khác, thì chỉ duy nhất một người trúng thưởng nhận đủ 6 phần của Jackpot 2 (100% giá trị)</li>
				</ul>
				<ul class="list-unstyled">
					<li><b>Xem hướng dẫn chơi</b> <a href="#">tại đây</a></li>
					<li><b>Xem thống kê</b> <a href="#">tại đây</a></li>
					<li><b>Thể lệ Power 6/55</b> <a href="#">tại đây</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php include('./include/footer.php')  ?>