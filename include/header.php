<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.9.0/css/all.css"
    />
    <link rel="stylesheet" href="./assets/css/vendors/bootstrap.min.css" />
	  <link rel="stylesheet" href="./assets/css/styles.css" />	
	
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Home</title>
  </head>
  <body>
  <div id="wrapper">

<?php include('./include/sidebar.php') ?>
<!-- Page Content -->
<div id="page-content-wrapper">

	<!-- Navbar top -->
	
	<?php include('./include/navbar.php') ?>
	<!-- end navbar top -->
	