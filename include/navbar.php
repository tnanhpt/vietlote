<div class="px-2 px-sm-3 result py-2 py-lg-3 bg-white menu_top">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 order-lg-last">
				<div class="row align-items-center justify-content-end">
					<div class="col-auto">
						<button class="navbar-toggler p-0 border-0 d-block d-lg-none" type="button" data-toggle="offcanvas">
							<i class="fas fa-bars fa-lg"></i>
						</button>
					</div>
					<div class="col-3 col-sm-2 mr-auto d-block d-lg-none">
						<img src="./assets/images/common/Logo.png" class="img-fluid" alt="logo vietlote">
					</div>
					<!-- not login -->
					<!-- <div class="col-auto">
						<a href="#" class="btn btn-user btn-signup ml-2">Đăng ký</a>
						<a href="#" class="btn btn-user btn-login ml-2">Đăng nhập</a>
					</div> 		 -->
					<!-- end not login -->
					<div class="col-auto">
						<div class="btn-toolbar">
							<div class="dropdown mr-2">
								<button type="button" data-toggle="dropdown" class="btn btn-light">
									<i class="far fa-bell fz-20 pointer">
										<div class="alert_count">29</div>
									</i>
								</button>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item text-truncate-2" href="#">Lorem ipsum, dolor sit amet consectetur adipisicing
										elit. Commodi eius quidem</a>
									<a class="dropdown-item text-truncate-2" href="#">Lorem, ipsum dolor sit amet consectetur adipisicing
										elit. Impedit quo quasi rerum sunt suscipit et earum quae reprehenderit nam. Maiores.</a>
									<a class="dropdown-item text-truncate-2" href="#">Lorem, ipsum dolor sit amet consectetur adipisicing
										elit. Impedit quo quasi rerum sunt suscipit et earum quae reprehenderit nam. Maiores.</a>
								</div>
							</div>
							<div class="dropdown">
								<button type="button" data-toggle="dropdown" class="btn btn-light">
									<i class="far fa-user fz-20 pointer"></i>
								</button>
								<div class="dropdown-menu dropdown-menu-right dropdown-alert alert_custom">
									<a class="dropdown-item text-truncate-2" href="#">Thông tin tài khoản</a>
									<a class="dropdown-item text-truncate-2" href="#">Nhật ký hoạt động</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item text-truncate-2" href="#">Đăng xuất</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-auto d-none d-lg-block">
						<img src="./assets/images/common/Vietlott_logo.png" class="img-fluid" alt="logo vietlote" />
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6 mr-auto order-lg-first">
				<div class="search_box">
					<form class="form-inline my-2 my-lg-0 position-relative w-100">
						<input class="form-control search_input w-100" type="text">
						<button class="btn btn-search my-sm-0" type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>