<footer id="footer">
	<div class="px-2 px-sm-3">
		<div class="container">
			<!-- <img src="./assets/images/common/vietlott_logo_footer.png" alt="" class="d-block mx-auto"> -->
			<ul class="flex-wrap text-center list-inline">
				<li class="list-inline-item"><a href="#">Giới thiệu</a></li>
				<li class="list-inline-item"><a href="#">Trải nghiệm Keno</a></li>
				<li class="list-inline-item"><a href="#">Keno cùng người nổi tiếng</a></li>
				<li class="list-inline-item"><a href="#">Tìm điểm bán</a></li>
				<li class="list-inline-item"><a href="#">Trở thành đại lý</a></li>
				<li class="list-inline-item"><a href="#">Các sản phẩm khác</a></li>
				<li class="list-inline-item"><a href="#">Liên hệ</a></li>
				<li class="list-inline-item"><a href="#">Chơi có trách nhiệm</a></li>
				<li class="list-inline-item"><a href="#">Đối tác</a></li>
			</ul>
			<div class="row mt-4">
				<div class="col-lg-3 col-6 mb-4">
					<div class="box_footer d-flex align-items-center">
						<ul class="list-unstyled w-100 m-0">
							<li class="media p-2">
								<img src="assets/images/icons/18.png" class="align-self-center mr-2" alt="">
								<div class="media-body align-self-center">
									<p>Dành cho người trên 18 tuổi</p>
								</div>
							</li>
							<li class="media p-2">
								<img src="assets/images/icons/security.png" class="align-self-center mr-2" alt="">
								<div class="media-body align-self-center">
									<p>Chơi game có trách nhiệm</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-6 mb-4">
					<div class="box_footer d-flex align-items-center">
						<div class="text-center w-100">
							<img src="./assets/images/common/Logo-sm.png" class="img-fuid" />
							<p class="text-uppercase mt-2 text-or">Quay thưởng siêu nhanh</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-6 mb-4">
					<div class="box_footer d-flex align-items-center">
						<div class="text-center w-100">
							<img src="./assets/images/common/Logo-sm.png" class="img-fuid" />
							<p class="text-uppercase mt-2 text-or">Cơ hội trở thành tỷ phú</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-6 mb-4">
					<div class="box_footer d-flex align-items-center">
						<div class="text-center w-100">
							<img src="./assets/images/common/Logo-sm.png" class="img-fuid" />
							<p class="text-uppercase mt-2 text-or">Cách chơi linh hoạt</p>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="row justify-content-between py-3 mt-4">
				<div class="col-md-6 txt_footer">
					<p>© 2018, Công ty Xổ Số Điện Toán Việt Nam - Bộ Tài Chính.</p>
					<p>Giấy phép: Số 3380/GP-TTĐT do Sở Thông tin và Truyền thông Hà Nội cấp ngày 30/8/2018</p>
					<p>Người chịu trách nhiệm: Tổng Giám Đốc Nguyễn Thanh Đạm.</p>
				</div>
				<div class="col-md-6 txt_footer">
					<p>Địa chỉ : Tầng 15, Tòa nhà CornerStone, 16 Phan Chu Trinh, Quận Hoàn Kiếm, Hà Nội</p>
					<p>Tổng đài Chăm sóc khách hàng: 1900.55.88.89</p>
					<p>Điện thoại: 024.62.686.818 - Email: contact@vietlott.vn</p>
				</div>
			</div>
		</div>
	</div>
	<div class="back_to_top" id="toTop"></div>	
</footer>

<!-- /#page-content-wrapper -->
</div>
<div class="layout-fixleft">
	<a href="#"><img src="./assets/images/home/god-fixed-2.png" alt="" class="god_fixed mb-2"></a>
	<button class="btn btn-fixed btn-call mb-2"><i class="fab fa-facebook"></i></button>
	<button class="btn btn-fixed btn-location"><span class="txt_vertical">Tìm điểm bán</span> <i
			class="fas fa-map-marker-alt"></i> </button>
</div>
<div class="menu-right-overlay" data-toggle="offcanvas"></div>
<div class="loading"></div>
<script src="./assets/js/jquery.min.js"></script>
<script src="./assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="../assets/plugins/slick/slick.min.js"></script>
<script src="./assets/js/custom.js"></script>
<script src="./assets/js/jquery.flipper-responsive.js"></script>
<script>

	jQuery(function ($) {
		$('#myFlipper').flipper('init');
		$('#modalFlipper').flipper('init');
	});

	$(function () {
		'use strict'

		$('.variable-width').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			arrows: false
		});
     

		$('[data-toggle="offcanvas"]').on('click', function () {
			$('.offcanvas-collapse').toggleClass('open')
		})
	})
	
</script>
</body>

</html>