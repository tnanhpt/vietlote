<!-- Sidebar -->
<div class="offcanvas-collapse" id="sidebar-wrapper">
  <div class="sidebar-heading px-4 py-3">
    <div class="d-flex">
      <button class="navbar-toggler p-0 mr-4 d-block d-lg-none border-0" type="button" data-toggle="offcanvas"
        style="color: #fff">
        <i class="fas fa-bars fa-lg"></i>
      </button>
      <a href="#">
        <img src="./assets/images/common/Logo.png" class="img-fluid" alt="Keno xổ số tự chọn" />
      </a>
    </div>
  </div>
  <div class="list-group list-group-flush">
    <a href="home-page.php" class="list-group-item list-group-item-action text-white menu-active">Trang chủ</a>
    <a href="#" class="list-group-item list-group-item-action text-white">Chơi thử</a>
    <a href="huong-dan-the-le.php" class="list-group-item list-group-item-action text-white">Thể lệ KENO</a>
    <a href="tim-diem-ban.php" class="list-group-item list-group-item-action text-white">Tìm điểm bán KENO</a>
    <a href="cac-san-pham-khac.php" class="list-group-item list-group-item-action text-white">Các sản phẩm khác</a>
    <a href="choi-co-trach-nhiem.php" class="list-group-item list-group-item-action text-white">Chơi có trách nhiệm</a>
    <a href="doi-tac.php" class="list-group-item list-group-item-action text-white">Đối tác</a>
    <a href="thong-tin-lien-he.php" class="list-group-item list-group-item-action text-white">Liên hệ</a>
    <!-- <a href="#" class="list-group-item list-group-item-action text-white">Keno cùng người nổi tiếng</a> -->
    <!-- <a href="#" class="list-group-item list-group-item-action text-white">Trở thành đại lý</a> -->
  </div>
  <div class="social d-flex justify-align-center align-items-center">
    <div class="icon_fb">
      <img src="./assets/images/icons/fb.png" />
      <div class="tip tip-fb">
        <i class="icon_tip icon_speak"></i>
        <div class="txt_speak fw-600 fz-15">
          Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
        </div>
      </div>
    </div>
    <div class="icon_zalo">
      <img src="./assets/images/icons/zalo.png" />
      <div class="tip tip-zalo">
        <i class="icon_tip icon_speak"></i>
        <div class="txt_speak fw-600 fz-15">
          Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
        </div>
      </div>
    </div>
    <div class="icon_youtube">
      <img src="./assets/images/icons/youtube.png" />
      <div class="tip tip-youtube">
        <i class="icon_tip icon_speak"></i>
        <div class="txt_speak fw-600 fz-15">
          Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /#sidebar-wrapper -->